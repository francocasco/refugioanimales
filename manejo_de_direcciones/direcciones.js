const axios = require('axios');

// Función asincrónica para obtener direcciones normalizadas
async function obtenerDirecciones(direccion) {
  const url = `http://servicios.usig.buenosaires.gob.ar/normalizar/?direccion=${encodeURIComponent(direccion)}`;

  try {
    const response = await axios.get(url);
    const data = response.data;
    // Guardar el contenido en una variable
    const direcciones = data.direccionesNormalizadas;
    return direcciones;
  } catch (error) {
    console.error('Error al obtener las direcciones normalizadas:', error);
    throw error;
  }
}

// Función para mostrar las direcciones
async function mostrarDirecciones(req, res) {
    try {
      // Aquí se realiza la lógica para obtener las direcciones
      const direcciones = await obtenerDirecciones(req.query.dir); // Ejemplo: obtenerDirecciones() devuelve una promesa
      res.render('seleccionar_direccion', {
        direcciones: direcciones
      });
    } catch (error) {
      console.error('Error al mostrar las direcciones:', error);
      res.render('seleccionar_direccion', {
        direcciones: []
      });
    }
  }
  
// Función para buscar direcciones
function buscarDirecciones(req, res) {
    mostrarDirecciones(req, res);
}

// Función para guardar la dirección seleccionada
function guardarDireccionSeleccionada(req, res) {
  const selectedAddress = req.body.addDireccion;
  // Redirige de vuelta al formulario de agregar organización, con la dirección seleccionada
  res.redirect('/anadir_organizacion?seleccionar_direccion=' + encodeURIComponent(selectedAddress));
}

module.exports = {
    mostrarDirecciones,
    buscarDirecciones,
    guardarDireccionSeleccionada
};
