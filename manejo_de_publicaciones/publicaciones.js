const fs = require('fs');
const path = require('path');

// Función para obtener publicaciones desde archivos
function obtenerPublicacionesDesdeArchivos() {
    const directorioPublicaciones = path.join(__dirname, '../publicaciones');
    const archivos = fs.readdirSync(directorioPublicaciones);
    const publicaciones = [];

    archivos.forEach(archivo => {
        const contenidoArchivo = fs.readFileSync(path.join(directorioPublicaciones, archivo), 'utf8');
        const publicacion = JSON.parse(contenidoArchivo);
        publicaciones.push(publicacion);
    });

    return publicaciones;
}

// Función para mostrar las publicaciones
function mostrarPublicaciones(req, res) {
    const publicaciones = obtenerPublicacionesDesdeArchivos();

    res.render('publicaciones', {
        publicaciones: publicaciones
    });
}

// Función para buscar publicaciones por palabras clave
function buscarPublicaciones(req, res) {
    const keywords = req.query.keywords;
    const publicaciones = obtenerPublicacionesDesdeArchivos();
    const filteredPublicaciones = publicaciones.filter(publicacion => {
        const razaMatch = publicacion.raza.toLowerCase().includes(keywords.toLowerCase());
        const caracteristicasMatch = publicacion.caracteristicas.toLowerCase().includes(keywords.toLowerCase());
        return razaMatch || caracteristicasMatch;
    });

    res.render('publicaciones', {
        publicaciones: filteredPublicaciones
    });
}

module.exports = {
    mostrarPublicaciones,
    buscarPublicaciones
};
