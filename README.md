# Refugio de Animales

Esta es la prueba de concepto del proyecto "Refugio de Animales", que implementa las funcionalidades para proporcionar información sobre organizaciones, publicaciones y permitir añadir nuevas organizaciones. El proyecto está desarrollado en JavaScript utilizando EJS para generar dinámicamente las vistas HTML en el servidor, y CSS para el estilo y diseño de las páginas web.

## Instalación y configuración

1. Instala node.js
2. Navega hasta la carpeta raíz del proyecto donde se encuentra el archivo app.js.
3. Inicia el servidor local: `node app`
4. Accede a la aplicación en tu navegador web: `http://localhost:3000`

## Características

- Muestra información sobre diferentes organizaciones de refugio de animales.
- Permite buscar organizaciones por servicios o actividades.
- Muestra publicaciones de mascotas perdidas, en adopción y encontradas.
- Permite buscar publicaciones por raza o características.
- Permite añadir nuevas organizaciones, normalizando la dirección.
- Permite mostrar en un mapa las direcciones de las organizaciones.

## Herramientas utilizadas

- Node.js: Entorno de ejecución de JavaScript utilizado para ejecutar JavaScript en el lado del servidor.
- Express.js: Framework de Node.js utilizado para crear aplicaciones web y APIs de manera sencilla y rápida.
- EJS: Motor de plantillas utilizado para generar dinámicamente el HTML en el servidor utilizando JavaScript.
- CSS: Lenguaje de estilos utilizado para dar estilo y diseño a las páginas web.
- Bootstrap: Framework de CSS utilizado para el diseño responsivo y los componentes de la interfaz de usuario, ampliamente utilizado en el proyecto.
- JavaScript: Lenguaje de programación utilizado para agregar interactividad y funcionalidad a las páginas web.
- Leaflet: Biblioteca de JavaScript utilizada para mostrar mapas interactivos en la aplicación.
- Usig: API que permite estandarizar y validar direcciones.

## Interacción entre Node.js y EJS

En este proyecto, Node.js y EJS trabajan en conjunto para crear una aplicación web dinámica. Node.js actúa como el servidor web y proporciona el entorno de ejecución para ejecutar código JavaScript en el servidor. Por otro lado, EJS se utiliza como un motor de plantillas que nos permite generar contenido HTML dinámico en el servidor. Utilizando las plantillas EJS, podemos incrustar código JavaScript en las páginas HTML y generar contenido personalizado basado en los datos recuperados. De esta manera, Node.js y EJS permiten construir una aplicación web interactiva y responder a las solicitudes de los clientes con páginas web dinámicas y personalizadas.
