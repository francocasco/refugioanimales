const fs = require('fs');
const path = require('path');

// Función para obtener organizaciones desde archivos
function obtenerOrganizacionesDesdeArchivos() {
  const directorioOrganizaciones = path.join(__dirname, '../organizaciones');
  const archivos = fs.readdirSync(directorioOrganizaciones);
  const organizaciones = [];

  archivos.forEach(archivo => {
    const contenidoArchivo = fs.readFileSync(path.join(directorioOrganizaciones, archivo), 'utf8');
    const organizacion = JSON.parse(contenidoArchivo);
    organizaciones.push(organizacion);
  });

  return organizaciones;
}

// Función para mostrar las organizaciones
function mostrarOrganizaciones(req, res) {
  const organizaciones = obtenerOrganizacionesDesdeArchivos();

  res.render('organizaciones', {
    organizaciones: organizaciones
  });
}

// Función para obtener el número máximo de organización creada
function obtenerNumeroMaximoOrganizacion() {
  const directorioOrganizaciones = path.join(__dirname, '../organizaciones');
  const archivos = fs.readdirSync(directorioOrganizaciones);
  let numeroMaximo = 0;

  archivos.forEach(archivo => {
    const numero = parseInt(archivo.match(/organizacion(\d+)\.json/)[1]);
    if (numero > numeroMaximo) {
      numeroMaximo = numero;
    }
  });

  return numeroMaximo;
}

// Función para agregar una organización
function agregarOrganizacion(req, res) {
  const orgData = {
    nombre: req.body.addOrgName,
    direccion: req.body.addOrgDireccion,
    hora_inicio: req.body.addOrgHoraInicio,
    hora_final: req.body.addOrgHoraFinal,
    descripcion: req.body.addOrgDescripcion,
    actividades: req.body.addOrgActividades
  };

  // Obtiene el número máximo de organización utilizado
  const maximoNumeroOrganizacion = obtenerNumeroMaximoOrganizacion();

  // Genera el número para la nueva organización
  const nuevoNumeroOrganizacion = maximoNumeroOrganizacion + 1;

  // Genera el nombre del archivo
  const fileName = `organizacion${nuevoNumeroOrganizacion}.json`;
  const filePath = path.join(__dirname, '..', 'organizaciones', fileName);

  // Convierte los datos a formato JSON
  const jsonData = JSON.stringify(orgData, null, 2);

  // Escribe los datos en el archivo JSON
  fs.writeFileSync(filePath, jsonData);

  res.send('Organización agregada correctamente');
}

// Función para buscar organizaciones por palabras clave
function buscarOrganizaciones(req, res) {
  const keywords = req.query.keywords;
  const organizaciones = obtenerOrganizacionesDesdeArchivos();
  const filteredOrganizaciones = organizaciones.filter(organizacion => {
    const actividadesMatch = organizacion.actividades.toLowerCase().includes(keywords.toLowerCase());
    const descripcionMatch = organizacion.descripcion.toLowerCase().includes(keywords.toLowerCase());
    return actividadesMatch || descripcionMatch;
  });

  res.render('organizaciones', {
    organizaciones: filteredOrganizaciones
  });
}

// Función para mostrar el formulario de agregar organización
function mostrarFormularioAgregarOrganizacion(req, res) {
  const selectedAddress = req.query.seleccionar_direccion || '';
  res.render('anadir_organizacion', { selectedAddress: selectedAddress });
}

module.exports = {
  mostrarOrganizaciones,
  agregarOrganizacion,
  buscarOrganizaciones,
  mostrarFormularioAgregarOrganizacion
};

