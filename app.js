const express = require('express');
const app = express();
const publicaciones = require('./manejo_de_publicaciones/publicaciones');
const organizaciones = require('./manejo_de_organizaciones/organizaciones');
const direcciones = require('./manejo_de_direcciones/direcciones');

// Configuración para leer datos del formulario
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// Configuración de dotenv
const dotenv = require('dotenv');
dotenv.config({ path: './env/.env' });

// Configuración del directorio público
app.use('/resources', express.static('public'));
app.use('/resources', express.static(__dirname + '/public'));

// Configuración del motor de plantillas
app.set('view engine', 'ejs');

// Ruta de la página de inicio
app.get('/', (req, res) => {
    res.render('index', {
        login: false,
        name: 'Debe iniciar sesión'
    });
});

// Ruta para mostrar publicaciones
app.get('/publicaciones', publicaciones.mostrarPublicaciones);

// Ruta para búsqueda por palabras clave de publicaciones
app.get('/search', publicaciones.buscarPublicaciones);

// Ruta para mostrar las organizaciones
app.get('/organizaciones', organizaciones.mostrarOrganizaciones);

// Ruta para búsqueda por palabras clave de organizaciones
app.get('/search_organizaciones', organizaciones.buscarOrganizaciones);

// Ruta para búsqueda de direcciones
app.get('/buscar_direcciones', direcciones.buscarDirecciones);

// Ruta para mostrar el formulario de agregar organización
app.get('/anadir_organizacion', organizaciones.mostrarFormularioAgregarOrganizacion);

// Ruta para mostrar las direcciones
app.get('/seleccionar_direccion', direcciones.mostrarDirecciones);

// Ruta para agregar una organización
app.post('/anadir_organizacion', organizaciones.agregarOrganizacion);

// Ruta para guardar la dirección seleccionada
app.post('/guardar_direccion', direcciones.guardarDireccionSeleccionada);

app.listen(3000, () => {
    console.log('Server running at http://localhost:3000');
});
