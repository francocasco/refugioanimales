function mostrarUbicacion(direccion) {

    const partes = direccion.split(' ');
    const longitud = parseFloat(partes[partes.length - 2]);
    const latitud = parseFloat(partes[partes.length - 1]);

    console.log('Latitud:', latitud);
    console.log('Longitud:', longitud);

    document.getElementById('mapModal').addEventListener('shown.bs.modal', function () {
        console.log(direccion);

        const map = L.map('map-template').setView([latitud, longitud], 13);
        L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: 19,
            attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
        }).addTo(map);

        const marker = L.marker([latitud, longitud]).addTo(map);
    });
}

document.getElementById('mapModal').addEventListener('hidden.bs.modal', function () {
    location.reload();
});


